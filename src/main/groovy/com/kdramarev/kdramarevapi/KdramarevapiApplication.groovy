package com.kdramarev.kdramarevapi

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

@SpringBootApplication
class KdramarevapiApplication extends SpringBootServletInitializer {

	static void main(String[] args) {
		SpringApplication.run KdramarevapiApplication, args
	}
}
