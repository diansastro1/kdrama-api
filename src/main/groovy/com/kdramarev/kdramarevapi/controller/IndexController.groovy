package com.kdramarev.kdramarevapi.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class IndexController {
    @RequestMapping('/')
    ResponseEntity<?> index(){
        return ResponseEntity.ok('kdramarev API')
    }
}
